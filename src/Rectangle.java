public class Rectangle {
    private double width;
    private double heigh;

    public Rectangle(double width, double heigh) {
        this.width = width;
        this.heigh = heigh;
    }

    //Area Rectangle
    public double getAreaRect() {
        return width * heigh;
    }

    //Circumference Rectangle
    public double getCircumRect(){
        return 2 * (width + heigh);
    }
}
