public class TestShape {
    public static void main(String[] args) {
        // Rectangle
        Rectangle rect1 = new Rectangle(10, 5);
        System.out.println(rect1.getAreaRect());
        System.out.println(rect1.getCircumRect());

        Rectangle rect2 = new Rectangle(5, 3);
        System.out.println(rect2.getAreaRect());
        System.out.println(rect2.getCircumRect());

        // Circle
        Circle circle1 = new Circle(1);
        System.out.println(circle1.getAreaCircle());
        System.out.println(circle1.getCircumCiec());

        Circle circle2 = new Circle(2);
        System.out.println(circle2.getAreaCircle());
        System.out.println(circle2.getCircumCiec());

        // Triangle
        Triangle Triangle1 = new Triangle(5,5,6);
        System.out.println( Triangle1.getAreaTri());
        System.out.println( Triangle1.getCircumTri());
    }
}
