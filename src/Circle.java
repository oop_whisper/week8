public class Circle {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    // Area circle
    public double getAreaCircle() {
        return Math.PI * (r * r);
    }

    // Circumference Circle
    public double getCircumCiec() {
        return 2 * (3.14 * r);
    }
}
