public class Triangle {
    private double a;
    private double b;
    private double c;
    private double s;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Area Triangle
    public double getAreaTri() {
        s = (a + b + c) / 2;
        return Math.sqrt(s * ((s - a) * (s - b) * (s - c)));
    }

    // Circumference Triangle
    public double getCircumTri() {
        return a + b + c;
    }
}
