public class Map1 {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int width_MIN1 = 0;
    public final static int width_MAX1 = 5;
    public final static int height_MIN1 = 0;
    public final static int height_MAX1 = 5;

    public Map1(String name, char symbol, int x, int y){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;

    }

    public Map1(String name, char symbol){
        this(name, symbol, 0, 0);
    }

    public char getSymbol(){
        return symbol;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public static void println() {
    }
}
